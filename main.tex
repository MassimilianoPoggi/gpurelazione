\documentclass{article}
\usepackage[utf8]{inputenc}

\title{Relazione progetto GPU computing}
\author{Massimiliano Poggi e Chiara Franceschetti}
\date{Gennaio 2019}

\usepackage{natbib}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage[hidelinks]{hyperref}
\usepackage[italian]{babel}
\usepackage{colortbl}
\usepackage{tikz}
\usepackage{boldline}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{tcolorbox}
\usepackage{multirow}
\usepackage{enumitem}
\usepackage{pgfplots}

\pgfplotsset{compat=1.15}

\newcommand{\lstfont}[1]{\color{#1}\scriptsize\ttfamily}

\lstset{
    language=[ANSI]C++,
    showstringspaces=false,
    backgroundcolor=\color{white},
    basicstyle=\lstfont{black},
    identifierstyle=\lstfont{black},
    keywordstyle=\lstfont{magenta!40},
    numberstyle=\lstfont{blue},
    stringstyle=\lstfont{cyan},
    commentstyle=\lstfont{yellow!30},
    emph={
        cudaMalloc, cudaFree,
        __global__, __shared__, __device__, __host__,
        __syncthreads,
    },
    emphstyle={\lstfont{green!60!white}},
    breaklines=true
}

\usetikzlibrary{arrows.meta}

\definecolor{selectiveyellow}{rgb}{1.0, 0.73, 0.0}
\definecolor{guppiegreen}{rgb}{0.0, 1.0, 0.5}
\definecolor{bittersweet}{rgb}{1.0, 0.44, 0.37}
\definecolor{brightturquoise}{rgb}{0.03, 0.91, 0.87}
\definecolor{bubblegum}{rgb}{0.99, 0.76, 0.8}
\definecolor{brightlavender}{rgb}{0.75, 0.58, 0.89}

\tikzset{every node/.style={circle,thick,draw}}

\begin{document}

\maketitle

\section{Colorazione di grafi}
%definizione di graph coloring
Dato un grafo $G=\langle V, E\rangle$, dove $V$ è l'insieme dei vertici ed $E$ è l'insieme degli archi, e dato un insieme di colori $C$, si definisce colorazione di un grafo una funzione $c: V \rightarrow C$ tale che $\forall(u, v)\in E\ \ c(u)\neq c(v)$, ovvero la colorazione assegna colori diversi a nodi adiacenti.

%su che problemi reali viene mappato (a cosa serve)
Il problema di colorazione di grafi ha svariate applicazioni pratiche.
Tra queste troviamo, ad esempio:
\begin{itemize}
    \item register allocation (più in generale assegnamento di risorse) \cite{Chaitin1981RegisterAV}: dato un insieme di istruzioni di codice che utilizzano variabili simboliche, si vuole assegnare a ciascuna in modo ottimale il registro in cui deve essere allocata. Per fare ciò, si costruisce il grafo delle dipendenze tra variabili, che ha per nodi le singole variabili e un arco fra due nodi se le variabili coesistono in uno stesso istante.
    La colorazione di un grafo così definito determina l'assegnamento di un registro ad un determinato sottoinsieme di variabili, dove ad ogni colore viene associato un singolo registro.
    \item task scheduling: dato un insieme di attività che competono per un insieme di risorse ad accesso esclusivo, si vuole assegnare una fascia di esecuzione alle singole attività in modo ottimale. Analogamente all'esempio precedente, si costruisce  un grafo avente per vertici le attività e un arco se due attività necessitano di una stessa risorsa. La colorazione di tale grafo determina la fascia di esecuzione assegnata ad ogni attività, dove colori diversi determinano istanti diversi. 
\end{itemize}
%perche è un problema difficile
Nonostante esistano alcuni casi risolvibili in tempo polinomiale, tra cui la colorazione di grafi perfetti \cite{Kratochvl1997ColoringPP} e quindi di casi più specifici quali grafi bipartiti, il problema della colorazione di grafi è, nel caso generale, un problema NP-hard \cite{Garey1979ComputersAI}. 
%perche viene parallelizzato
Al fine di rendere trattabili le istanze del problema, a livello pratico è necessario optare per soluzioni approssimate dello stesso e ricorrere all'utlizzo di algoritmi risolutivi che si prestano alla parallelizzazione della computazione. 


\section{Algoritmi di risoluzione}
\label{sect:algo}
%CPU
\subsection{Algoritmi sequenziali}
\label{sect:algo_sequenziale}
La ricerca sulla colorazione sequenziale di grafi è piuttosto vasta. L'idea di base per la costruzione di un algoritmo risolutivo è la seguente:
\begin{itemize}[itemsep=2pt,topsep=2pt]
    \item si stabilisce un ordinamento dei nodi
    \item si scorrono i nodi nell'ordine stabilito: ad ogni nodo si assegna il minimo colore disponibile in base alla colorazione dei nodi ad esso adiancenti
\end{itemize}
Un esempio di esecuzione di questo algoritmo è mostrato nella figura \ref{fig:algo_sequenziale}: la numerazione dei nodi rappresenta l'ordinamento scelto.
Si noti che eseguendo un algoritmo così definito, la colorazione del grafo dipende fortemente dal prestabilito ordinamento iniziale dei nodi.
Per ogni grafo esiste un ordinamento dei nodi che ne produce la colorazione ottima \cite{GCalgo}. Questo presupposto ha dato origine ad una vasta varietà di algoritmi per la colorazione di grafi, distinti perlopiù dalla strategia di ordinamento iniziale scelta \cite{seqGraph}.

\input{sequentialexample}

\subsection{Algoritmi paralleli}
\label{sect:algo_parallelo}
%GPU in generale + algo specifici
La complessità del problema ha portato allo sviluppo di algoritmi risolutivi paralleli.
Esistono svariati algoritmi \cite{AllwrightComparison}, basati come nel caso sequenziale su un'idea semplice che viene poi sviluppata con tecniche diverse:
\begin{itemize}
	\item si trova un insieme di nodi indipendenti del grafo, ovvero un sottoinsieme $V' \subseteq V$ di vertici tale per cui $\forall u,v \in V' \ (u,v) \notin E$
	\item si assegna un colore a ciascun vertice dell'insieme indipendente
	\item si ripete il procedimento sul grafo ignorando i nodi già colorati
\end{itemize}

La generazione dell'insieme indipendente in parallelo si basa sul trovare i massimi locali in base ad una funzione di costo predeterminata, ed è la scelta di quest'ultima che differenzia i vari algoritmi qui studiati.

Saranno presentate in particolare quattro funzioni di costo associate ai nodi:
\begin{itemize}
	\item l'indice del nodo stesso all'interno del grafo
	\item estrazione casuale ad ogni iterazione dell'algoritmo
	\item estrazione casuale una volta sola all'inizio dell'algoritmo
	\item il grado del nodo stesso all'interno del grafo
\end{itemize}

\input{parallelexample}
	
\section{Strategie utilizzate}

\subsection{Rappresentazione del grafo}
%accessi coalescenti
Per facilitare la comprensione delle strutture usate si faccia riferimento al grafo in figura \ref{fig:graph_image}, utilizzato già nella sezione \ref{sect:algo} come esempio per l'esecuzione degli algoritmi. All'interno di ogni nodo viene qui indicato l'indice corrispondente al nodo nella struttura dati e i nodi sono colorati per migliorare la comprensione della struttura.

\input{graph_image}

\`{E} stata scelta una rappresentazione tipica per i grafi sparsi, scrivendo esplicitamente le liste di adiacenza dei singoli nodi. Per mantenere accessi coalescenti in memoria da parte dei kernel si è linearizzata la lista dei vicini e all'interno della struttura viene salvato il grado cumulato dei nodi (indice escluso), che funge da puntatore per l'accesso nella lista dei vicini come mostrato in figura \ref{fig:graph_representation}, dove la lista dei vicini di un determinato nodo ha lo stesso colore del nodo nella figura \ref{fig:graph_image}.

\input{graph_representation}

\clearpage
\subsection{Gestione dei nodi da colorare}
\label{sect:structure}
%struttura todo
In un primo momento si è utilizzata un'implementazione naive dell'algoritmo descritto nella sezione \ref{sect:algo_parallelo}, ma si è osservato che venivano lanciati moltissimi kernel che terminavano immediatamente perchè appartenenti a nodi già colorati e quindi superflui. A questo proposito si è deciso di utilizzare una struttura, nominata \textit{toDo}, che separa i nodi già colorati (su cui quindi non devono essere lanciati kernel) dai nodi non colorati (su cui invece bisogna continuare con l'algoritmo). Questa struttura e la sua macro evoluzione durante l'esecuzione di una singola iterazione sono mostrati nella figura \ref{fig:todo_evolution}.
Si noti che i nodi all'interno delle sottosezioni \textit{da colorare} e \textit{non colorati} sono mantenuti in ordine per garantire acessi ad aree di memoria quantomeno vicine (anche se non più perfettamente coalescenti).

\input{structure}

La figura \ref{fig:permutation} mostra invece in modo più approfondito come la struttura $toDo$ venga manipolata. In particolare, avendo come input la struttura $toDo$ iniziale e la struttura $toColor$ (un array che contiene 1 in corrispondenza dei nodi da colorare e 0 in corrispondenza di quelli da non colorare), viene prima calcolata la somma prefissa dell'array $toColor$. La somma prefissa gode di particolari proprietà che risultano utili nel calcolo della permutazione da applicare:
\begin{itemize}
	\item l'ultimo elemento della somma prefissa rappresenta il numero di nodi che devono essere colorati a questa iterazione
	\item ciascun elemento della somma prefissa indica quanti nodi sono stati colorati fino a quel punto (nodo compreso)
\end{itemize}
A questo punto si distinguono due casi, a seconda che il nodo sia da colorare o meno:
\begin{itemize}
	\item il nodo è da colorare (caselle interessate colorate di verde): si deve spostare il nodo verso la parte iniziale della struttura. In particolare il nodo deve andare alla posizione indicata dalla somma prefissa - 1 (perchè il nodo stesso è incluso), che mi indica quanti nodi da colorare sono già stati spostati.
	\item il nodo non è da colorare (caselle interessate colorate di arancione): si deve spostare il nodo dopo tutti quelli da colorare. Il numero di nodi da colorare è indicato dall'ultimo elemento della somma prefissa, a cui si deve aggiungere il numero di nodi già colorati che precedono quello corrente. Pur non avendo questo numero direttamente, è facile ricavarlo come differenza fra il $threadId$ (che rappresenta il numero di nodi che precedono quello corrente) e la somma prefissa (che rappresenta il numero di nodi da colorare che precedono quello corrente).
\end{itemize}
Con queste considerazioni, e utilizzando la moltiplicazione al posto di una struttura $if$ per evitare warp divergence nel calcolo della permutazione, si può derivare la seguente formula:
\begin{lstlisting}
permutation[threadId] = toColor[threadId] * (prefixSum[threadId] - 1) +
	(1 - toColor[threadId]) * (prefixSum[nNodes - 1] +
	threadId - prefixSum[threadId])
\end{lstlisting}

\input{permutation}

%shared
%constant

\clearpage
\section{Tempi di esecuzione}
In questa sezione vengono presentati i risultati sperimentali dell'applicazione delle varie funzioni di peso descritte nella sezione \ref{sect:algo_parallelo}.
I file contenenti i dati delle istanze sono reperibili nella directory \textit{files/graphs}: la prima riga del file contiene il numero di nodi (specificato due volte perchè il formato servirebbe per rappresentare in realtà una matrice sparsa) e il numero di archi, mentre ciascuna riga successiva rappresenta un arco del grafo.
Per comodità di lettura viene fornita una tabella che riassume le caratteristiche dei grafi utilizzati nella valutazione dei tempi di esecuzione.

\input{data/graph_info}

I tempi di esecuzione presentati in questa sezione sono reperibili all'interno dei file \textit{files/results/\textbf{algoritmo}/\textbf{numero thread}/\textbf{grafo}-results.txt}.

\subsection{Algoritmo sequenziale}

L'algoritmo CPU utilizzato è quello descritto nella sezione \ref{sect:algo_sequenziale}, utilizzando come funzione di peso per ordinare i nodi il grado dei nodi stessi. I grafi mancanti non sono stati misurati perchè il tempo di esecuzione per ciascuno superava i 30 minuti.

\input{data/greedy_cpu}

\clearpage
\subsection{Indice del nodo}
\subsubsection{Senza struttura}

\input{data/luby_simple_indexes}

\subsubsection{Con struttura}

\input{data/luby_struct_indexes}

\clearpage

\subsection{Peso random ad ogni iterazione}
\subsubsection{Senza struttura}

\input{data/luby_simple_random}

\subsubsection{Con struttura}

\input{data/luby_struct_random}

\clearpage
\subsection{Peso random all'inizio}
\subsubsection{Senza struttura}

\input{data/plassman_simple}

\subsubsection{Con struttura}

\input{data/plassman_struct}

\clearpage
\subsection{Grado del nodo}
\subsubsection{Senza struttura}

\input{data/degree_simple}

\subsubsection{Con struttura}

\input{data/degree_struct}

\clearpage

\subsection{Analisi dello speed-up}

\input{data/speedup_plot}

Sono rappresentati nel grafico \ref{fig:time_comparison} solamente i risultati relativi ai grafi generati casualmente, in quanto omogenei dal punto di vista della densità: si può comunque osservare uno speed-up considerevole.

\section{Colorazioni risultanti}

\input{data/colors}

\clearpage
\section{Metriche analizzate}

Sono state effettuate le misurazioni delle seguenti metriche, utilizzando come parametri una dimensione di blocco pari a 256 e come grafo \textit{delaunay\_n19}:
\begin{itemize}
	\item \textit{global memory load efficiency}: rapporto fra la memoria richiesta in lettura e quella effettivamente letta
	\item \textit{global memory store efficiency}: rapporto fra la memoria richiesta in scrittura e quella effettivamente scritta
	\item \textit{achieved occupancy}: rapporto fra il numero di warp supportati dalla GPU e il numero di warp attivi
	\item \textit{warp execution efficiency}: rapporto fra il numero di thread per warp supportati e il numero di thread per warp attivi
\end{itemize}

I risultati sono reperibili sotto \textit{files/results/\textbf{algoritmo}/metrics.txt}, in questa sezione ci si limiterà a commentarli.

Gli accessi in memoria ai metadati della struttura del grafo (numero di nodi, puntatore alla lista dei vicini e puntatore alla lista dei gradi cumulati) potrebbero essere migliorati spostando la struttura in \textit{constant memory} piuttosto che in \textit{unified memory}: si è mantenuta la seconda opzione per mantenere la compatibilità con il codice fornito.

Gli accessi in memoria ai colori dei vicini e alle varie funzioni di costo associate ai nodi sono disallineati e non coalescenti sia con che senza struttura perchè non è possibile anticipare la struttura del grafo in ingresso. Un discorso analogo avviene per la lista dei colori assegnati, in quanto non è possibile stabilire a priori in che ordine i nodi verranno colorati.

Le metriche riguardanti invece la gestione della struttura descritta nella sezione \ref{sect:structure} sono soddisfacenti: la warp efficiency media è vicina al 98\% e si osserva anche un aumento della warp efficiency dei kernel di colorazione dei nodi.

Il kernel che ha prestazioni peggiori all'interno di ciascun algoritmo è quello che seleziona i nodi da colorare all'iterazione corrente (chiamato \textit{isToColor}): questo risultato era atteso in quanto le liste dei vicini possono risultare estremamente disomogenee e nodi diversi richiederanno in generale la visita di un numero diverso di vicini per stabilire se è necessario colorare o meno.

\clearpage
\section{Conclusioni}

La maggior parte degli algoritmi performa in modo ottimale con una dimensione di blocco pari a 256 thread: unica eccezione è \textit{Luby index}, che ottiene risultati migliori con una dimensione di blocco pari a 32 thread. 

L'algoritmo che risulta avere sia il numero inferiore di colori che i tempi di esecuzioni migliori è \textit{Luby random}, che utilizza come funzione di costo un peso casuale associato ai nodi ed estratto ad ogni iterazione.

Osserviamo inoltre che gli algoritmi basati sull'indice del nodo e sul grado del nodo hanno prestazioni pessime su alcune istanze, in particolare \textit{rand200k} e \textit{channel}: si suppone che questo sia dovuto ad una particolare struttura dei grafi in questione, fattore che invece non incide sulle prestazioni degli algoritmi che associano pesi casuali.

La struttura \textit{toDo} aiuta per i grafi da 200mila nodi in poi: l'overhead risulta invece eccessivo per grafi di dimensione inferiore.

I tempi di esecuzione dell'algoritmo parallelo sono influenzati fortemente dal numero degli archi del grafo piuttosto che dal numero dei nodi: questo è compatibile con le misurazioni della warp efficiency che mostrano come il kernel più inefficiente sia quello che determina i nodi da colorare, che deve scorrere le liste dei vicini e quindi si presta poco alla parallelizzazione per via delle dimensioni e delle condizioni di uscita molto variabili.

\clearpage
\bibliographystyle{plain}
\bibliography{references}
\end{document}
